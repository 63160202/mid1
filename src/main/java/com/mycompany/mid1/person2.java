/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.mid1;

/**
 *
 * @author admin
 */
public class person2 {
    private String name;   //ประกาศ attributes name เป็น type string มี modifiers เป็น private
    private double price ;   //ประกาศ attributes price เป็น type double มี modifiers เป็น private
    private int amount ;    //ประกาศ attributes amount เป็น type int มี modifiers เป็น private
    private int total; //ประกาศ attributes total เป็น type int มี modifiers เป็น private
    
    public person2(String name) {  //ประกาศ constructor ของ class person2 รับมา 1 parameter
        this.name = name;  //เซ็ต parameter name เข้าไปใน attributes name
        this.price =0;  //เซ็ต parameter price เข้าไปใน attributes price เซ็ตค่าเป็น 0
        this.amount = 0;  //เซ็ต parameter amount เข้าไปใน attributes amount  เซ็ตค่าเป็น 0
    }
    
    public void numamount(int numpd) {  //นับจำนวนสินค้าทั้งหมด
        this.amount=numpd;  
        total+=numpd;
    }
    
    public void numprice(double numpd) {  //นับราคาทั้งหมด
        this.price=this.price+numpd;  
    }
    
    public void show() {  //สร้าง method show ไม่มีการ returnค่ากลับ เพื่อแสดงข้อมูลดังนี้ ได้แก่ ชื่อคนซื้อ, จำนวนปริมาณสินค้าที่ซื้อทั้งหมด , จำนวนราคาการซื้อสินค้าทั้งหมด
        System.out.println("name person : "+ this.name +"  number of products : " + this.total + "  total of products :" +this.price);
    }
    
    public void showamount() {  //สร้าง method show ไม่มีการ returnค่ากลับ เพื่อแสดงข้อมูลดังนี้ ได้แก่ ชื่อคนซื้อ, จำนวนปริมาณสินค้าที่ซื้อทั้งหมด , จำนวนราคาการซื้อสินค้าทั้งหมด
        System.out.println("number of products : "+ this.amount);
    }
    
    public String getName() {  //ประกาศ method getName return attributes name ออกไปเป็น string
        return name;
    }

    public double getPrice() {  //ประกาศ method getPrice return attributes price ออกไปเป็น int
        return price;
    }

    public int getAmount() {  //ประกาศ method getAmount return attributes amount ออกไปเป็น int
        return amount;
    }

    public int getTotal() {  //ประกาศ method getTotal return attributes total ออกไปเป็น int
        return total;
    }

}
