/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.mid1;

/**
 *
 * @author admin
 */
public class drink2 {
    private String name;  //ประกาศ attributes name เป็น type string มี modifiers เป็น protected
    private String taste;  //ประกาศ attributes taste เป็น type string มี modifiers เป็น protected
    private int amount;  //ประกาศ attributes amount เป็น type int มี modifiers เป็น protected
    private int price;  //ประกาศ attributes price เป็น type int มี modifiers เป็น protected
    
    public drink2(String name, String taste, int amount, int price) { //ประกาศ constructor ของ class drink1 รับมา 5 parameter
        this.name = name; //เซ็ต parameter name เข้าไปใน attributes name
        this.taste = taste;  //เซ็ต parameter tasteเข้าไปใน attributes taste
        this.amount = amount;  //เซ็ต parameter amount เข้าไปใน attributes amount
        this.price = price;  //เซ็ต parameter price เข้าไปใน attributes price
    }
    
    public void show() {  //สร้าง method show ไม่มีการ returnค่ากลับ เพื่อแสดงข้อมูลดังนี้ ได้แก่ ชื่อน้ำ รสชาติ ปริมาณ ราคา
        System.out.println("name : " + name+ " taste : " + taste + " amount : " + amount +" ML" + " price : " + price  );
    }
    
    public int calculateprice1() {  //สร้าง method calculateprice มีการ returnค่าprice เป็น int 
       return price;
    }
    
    public int calculateprice(int buyer) {  //สร้าง method calculateprice มีการ returnค่าprice เป็น int 
        return buyer*price;
    }
}
